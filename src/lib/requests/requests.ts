import { AssociationsApiFactory, Configuration, SponsorsApiFactory, WebsiteApiFactory } from '$lib/api';
import {
	StreamersApiFactory,
} from '$lib/api';
import { api } from '$lib/config';

export const streamerApi = () => {
	return StreamersApiFactory(
		new Configuration({
			basePath: api(),
		})
	);
};

export const sponsorApi = () => {
	return SponsorsApiFactory(
		new Configuration({
			basePath: api(),
		})
	);
};
export const associationsApi = () => {
	return AssociationsApiFactory(
		new Configuration({
			basePath: api(),
		})
	);
};
export const websiteApi = () => {
	return WebsiteApiFactory(
		new Configuration({
			basePath: api(),
		})
	);
};