export type Donor = {
    n: number;
    amount: number;
    first_name: string;
    id: string;
    last_name: string;
    time: number;
}
