export type Team = {
    name: string;
    color: string;
}

export const teams: Team[] = [
    { name: "blublublbublublbll", color: "#64b8f7" },
    { name: "FEUR 🏒", color: "#ed465b" },
    { name: "Diving Vector", color: "#fe8325" },
    { name: "Lila Legends", color: "#e292ff" },
    { name: "Stream Principal", color: "#5bbd4c" },
];