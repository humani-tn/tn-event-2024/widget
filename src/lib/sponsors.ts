type Sponsor = {
    description: string;
    id: number;
    image_file: string;
    link: string;
    name: string;
};